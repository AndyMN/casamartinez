CREATE TABLE alarm_states(
id integer primary key,
state int not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
CREATE TABLE sensor_locations(
id integer primary key,
rx_addr char(10) not null,
location varchar(30) not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
CREATE TABLE contact_info(
id integer primary key,
name varchar(30) not null,
tel_nr char(12) not null,
email varchar(64) not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
CREATE TABLE sensor_states(
id integer primary key,
rx_addr char(10) not null,
location varchar(30) not null,
state int not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
