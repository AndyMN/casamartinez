CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE apps(
id INTEGER PRIMARY KEY,
app_name varchar(32)
);
CREATE TABLE users(
user_id integer primary key,
user_name varchar(45) not null,
user_pass char(128) not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
CREATE TABLE auth_tokens(
id integer primary key,
selector char(24) not null,
validator char(64) not null,
user_id int not null,
app_name varchar(32) not null,
creation_time_unix int default (cast(strftime('%s', 'now', 'utc') as int)),
creation_time datetime default current_timestamp);
