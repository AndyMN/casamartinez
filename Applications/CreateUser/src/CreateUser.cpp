#include <iostream>
#include <unistd.h>
#include <string>

#include "AuthServerDatabaseHelper.h"
#include "CryptoHelper.h"

int main(int argc, char* argv[])
{

	if (argc < 2){
		std::cerr << "USAGE: " << argv[0] << " <DATABASE LOCATION>" << std::endl;
		return 1;
	}

	AuthServerDatabaseHelper dh(argv[1]);

	std::string user_name;
	std::string password_first_type;
	std::string password_second_type;

	// Ask user name
	std::cout << "Please enter a user name: ";
    std::cin >> user_name;

	// Check if user name is in database already

	if (dh.user_name_in_database(user_name)){

		std::cerr << "User name already exists in database !" << std::endl;
		return 1;
	}

	// Ask for password in an invisible prompt
	password_first_type = getpass("Enter Password: ");
	password_second_type= getpass("Retype Password: ");

	if (password_first_type != password_second_type){
		std::cerr << "Passwords do not match !" << std::endl;
		return 1;
	}

	// hash password with argon2
	CryptoHelper ch;
	std::string password_hash = ch.hash_password(password_first_type);

	dh.create_user_account(user_name, password_hash);

    return 0;
}
