#ifndef AUTHSERVERDATABASEHELPER_H
#define AUTHSERVERDATABASEHELPER_H

#include "DatabaseHelper.h"

class AuthServerDatabaseHelper: public DatabaseHelper{
public:
  AuthServerDatabaseHelper(std::string database_location);
  ~AuthServerDatabaseHelper();

  bool user_name_in_database(std::string user_name);
  std::string get_user_hashed_password(std::string user_name);

  bool app_name_in_database(std::string app_name);

  void create_user_account(std::string user_name, std::string hashed_password);

  int get_user_id(std::string user_name);
  bool auth_token_in_database(int user_id, std::string app_name);
  void create_auth_token(int user_id, std::string selector, std::string hashed_validator, std::string app_name);
  void remove_auth_token(int user_id, std::string app_name);

  bool selector_in_database(std::string selector, std::string app_name);
  void renew_auth_token(std::string selector, std::string hashed_validator);
  std::string get_selected_hashed_validator(std::string selector);

  const unsigned int get_hashed_password_size();

private:
  static const unsigned int _hashed_password_size = 124;
};

#endif // AUTHSERVERDATABASEHELPER_H
