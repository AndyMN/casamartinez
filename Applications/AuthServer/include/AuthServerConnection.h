#ifndef AUTHSERVERCONNECTION_H
#define AUTHSERVERCONNECTION_H


#include "BaseServerConnection.h"
#include "AuthServerDatabaseHelper.h"

class AuthServerConnection:public BaseServerConnection
{
public:
	AuthServerConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename, std::string database_location);
	~AuthServerConnection();

	void run();

private:
	AuthServerDatabaseHelper _dh;

};
#endif // AUTHSERVERCONNECTION_H
