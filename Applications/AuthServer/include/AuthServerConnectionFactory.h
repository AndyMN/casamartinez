#ifndef AUTHSERVERCONNECTIONFACTORY_H
#define AUTHSERVERCONNECTIONFACTORY_H

#include <string>

#include "Poco/Net/TCPServerConnectionFactory.h"
#include "AuthServerConnection.h"

template <class ServerConnection>
class AuthServerConnectionFactory : public Poco::Net::TCPServerConnectionFactory
{
public:
	AuthServerConnectionFactory(std::string aes_key_filename, std::string database_location)
		:_database_location(database_location),
		 _aes_key_filename(aes_key_filename){}

	~AuthServerConnectionFactory(){}

	AuthServerConnection* createConnection(const Poco::Net::StreamSocket& socket){
		return new ServerConnection(socket, _aes_key_filename, _database_location);
	}

private:
	std::string _database_location;
	std::string _aes_key_filename;
};



#endif // AUTHSERVERCONNECTIONFACTORY_H
