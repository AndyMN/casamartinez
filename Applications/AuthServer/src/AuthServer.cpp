#include <iostream>


#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/TCPServer.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/ServerApplication.h"

#include "AuthServerConnection.h"
#include "AuthServerConnectionFactory.h"
#include "toml.hpp"

class AuthServer: public Poco::Util::ServerApplication{

public:
	AuthServer(){}
	~AuthServer(){}

protected:
	void initialize(Poco::Util::Application& self){
		Poco::Util::Application::loadConfiguration();
		Poco::Util::ServerApplication::initialize(self);
	}

	void uninitialize(){
		Poco::Util::ServerApplication::uninitialize();
	}

	int main(const std::vector<std::string>& args){

		if (args.size() < 1){
			std::cerr << "Usage: <CONFIG LOCATION>"  << std::endl;
			return 1;
		}

		auto config_toml = toml::parse(args[0]);

		auto authserver_config = toml::find(config_toml, "authserver");
		int authserver_port = toml::find<int>(authserver_config, "port");
		
		auto database_config = toml::find(config_toml, "database");
		std::string database_location = toml::find<std::string>(database_config, "path");
		
		auto encryption_config = toml::find(config_toml, "encryption");
		std::string aes_key_filename = toml::find<std::string>(encryption_config, "aes_key_path");

		Poco::Net::ServerSocket ss(authserver_port);
		Poco::Net::TCPServer srv(new AuthServerConnectionFactory<AuthServerConnection>(aes_key_filename, database_location), ss);
		srv.start();
		std::cout << "Authentication Server Started." << std::endl;
		waitForTerminationRequest();
		srv.stop();
	}
};

int main(int argc, char* argv[])
{
	AuthServer myApp;
	return myApp.run(argc, argv);
}
