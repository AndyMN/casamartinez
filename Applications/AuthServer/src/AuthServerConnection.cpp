#include <iostream>

#include "BaseServerConnection.h"
#include "AuthServerDatabaseHelper.h"
#include "CasaMartinezException.h"
#include "CasaMartinezLogger.h"

#include "AuthServerConnection.h"

AuthServerConnection::AuthServerConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename, std::string database_location)
	:BaseServerConnection(s, aes_key_filename),
	 _dh(database_location){}

AuthServerConnection::~AuthServerConnection(){}

void AuthServerConnection::run(){

	CasaMartinezLogger cl("AuthServerLog.txt", "AuthServerErrorLog.txt");

	std::string incoming_connection_address = this->socket().peerAddress().host().toString();

	std::cout << "Connection from: " << incoming_connection_address << std::endl;

	cl.log(incoming_connection_address);

	try{

		std::string received_data = receive_data();

		std::vector<std::string> split_data = read_received_data(received_data);

		for (std::string s: split_data){
			std::cout << s << std::endl;
		}


		if (split_data.size() < 4){
			throw CasaMartinezException(56);
		}

		std::string& auth_method = split_data[0];
		std::string& user_name = split_data[1];
		std::string& user_credential = split_data[2];
		std::string& app_name = split_data[3];

		if ((auth_method != "unamepass") && (auth_method != "selectvalid")){
			throw CasaMartinezException(1);
		}

		if (!_dh.app_name_in_database(app_name)){
			throw CasaMartinezException(20);
		}

		if (auth_method == "unamepass"){
			if (!_dh.user_name_in_database(user_name)){
				throw CasaMartinezException(2);
			}

			int user_id = _dh.get_user_id(user_name);
			std::string hashed_database_password = _dh.get_user_hashed_password(user_name);

			if (!_ch.passwords_are_equal(user_credential, hashed_database_password)){
				throw CasaMartinezException(3);
			}
			
			std::cout << incoming_connection_address + "\t" + app_name + "\t" + "Logged in" << std::endl;
			cl.log(incoming_connection_address + "\t" + app_name + "\t" + "Logged in");

			if (_dh.auth_token_in_database(user_id, app_name)){
				_dh.remove_auth_token(user_id, app_name);
			}
			
			// Make new selector/validator pair
			std::pair<std::string, std::string> selector_validator_pair = _ch.generate_selector_validator_pair();
			std::string hashed_validator = _ch.SHA256_hash_string_to_hex(selector_validator_pair.second);

			// Create auth token
			_dh.create_auth_token(user_id, selector_validator_pair.first, hashed_validator, app_name);

			// Send selector/validator pair back
			std::string response = "OK\tselectvalid\t";
			response += selector_validator_pair.first;
			response += "\t";
			response += selector_validator_pair.second;
			this->send_data(response);
		}

		if (auth_method == "selectvalid"){
			if (!_dh.selector_in_database(user_name, app_name)){
				throw CasaMartinezException(4);
			}
			
			std::string hashed_validator_in_database = _dh.get_selected_hashed_validator(user_name);
			std::cout << hashed_validator_in_database << std::endl;
			if (!_ch.validators_are_equal(user_credential, hashed_validator_in_database)){
				throw CasaMartinezException(5);
			}
			
			std::cout << incoming_connection_address + "\t" + app_name + "\t" + "Correct auth token" << std::endl;
			cl.log(incoming_connection_address + "\t" + app_name + "\t" + "Correct auth token");

			// generate new validator
			std::string new_validator = _ch.generate_validator();
			std::string new_hashed_validator = _ch.SHA256_hash_string_to_hex(new_validator);
			// renew auth_token
			_dh.renew_auth_token(user_name, new_hashed_validator);
			// send new validator
			std::string response = "OK\tselectvalid\t";
			response += user_name;
			response += "\t";
			response += new_validator;
			this->send_data(response);
		}
	}catch (Poco::Exception& exc){
		std::cerr << incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << std::endl;
		cl.log_error(incoming_connection_address + "\t" + exc.displayText());
	}catch (CasaMartinezException& cmexc){
		std::cerr << incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << std::endl;
		this->send_data("ERROR\t" + std::to_string(cmexc.get_error_code()));
		cl.log_error(incoming_connection_address + "\t" + cmexc.get_error_message());
	}catch (std::exception& e){
		std::cerr << e.what() << std::endl;
		this->send_data("ERROR\tCPP");
		cl.log_error(incoming_connection_address + "\t" + e.what());
	}
}
