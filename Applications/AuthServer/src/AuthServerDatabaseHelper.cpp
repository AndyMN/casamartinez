#include <iostream>
#include <cstring>

#include "AuthServerDatabaseHelper.h"
#include "DatabaseHelper.h"
#include "CasaMartinezException.h"


AuthServerDatabaseHelper::AuthServerDatabaseHelper(std::string database_location)
	:DatabaseHelper(database_location){
}

AuthServerDatabaseHelper::~AuthServerDatabaseHelper(){
}

const unsigned int AuthServerDatabaseHelper::get_hashed_password_size(){
	return _hashed_password_size;
}

void AuthServerDatabaseHelper::create_auth_token(int user_id, std::string selector, std::string hashed_validator, std::string app_name){

	this->_prepare_statement("insert into auth_tokens (selector, validator, user_id, app_name) values (?, ?, ?, ?)");
	this->_bind_statement_text(1, selector.c_str());
	this->_bind_statement_text(2, hashed_validator.c_str());
	this->_bind_statement_int(3, user_id);
	this->_bind_statement_text(4, app_name.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	if (execute_status != SQLITE_DONE){
		throw CasaMartinezException(6, execute_status);
	}
}

void AuthServerDatabaseHelper::create_user_account(std::string user_name, std::string hashed_password){

	_prepare_statement("insert into users (user_name, user_pass) values (?, ?)");
	_bind_statement_text(1, user_name.c_str());
	_bind_statement_text(2, hashed_password.c_str());

	int execute_status = _execute_statement();

	this->_finalize_statement();

	if (execute_status != SQLITE_DONE){
		throw CasaMartinezException(7, execute_status);
	}
}

void AuthServerDatabaseHelper::remove_auth_token(int user_id, std::string app_name){

	this->_prepare_statement("delete from auth_tokens where user_id = ? and app_name = ?");
	this->_bind_statement_int(1, user_id);
	this->_bind_statement_text(2, app_name.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	if (execute_status != SQLITE_DONE){
		throw CasaMartinezException(8, execute_status);
	}
}

void AuthServerDatabaseHelper::renew_auth_token(std::string selector, std::string hashed_validator){

	this->_prepare_statement("update auth_tokens set validator = ?, creation_time = current_timestamp where selector = ?");
	this->_bind_statement_text(1, hashed_validator.c_str());
	this->_bind_statement_text(2, selector.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	if (execute_status != SQLITE_DONE){
		throw CasaMartinezException(9, execute_status);
	}
}

std::string AuthServerDatabaseHelper::get_selected_hashed_validator(std::string selector){

	this->_prepare_statement("select validator from auth_tokens where selector = ?");
	this->_bind_statement_text(1, selector.c_str());

	int execute_status = this->_execute_statement();

	if (execute_status != SQLITE_ROW){
		this->_finalize_statement();
		throw CasaMartinezException(10, execute_status);
	}else{
		const unsigned char* hashed_validator_badtype = sqlite3_column_text(_statement, 0);
     	std::string hashed_validator(reinterpret_cast<const char*>(hashed_validator_badtype));

		this->_finalize_statement();

		return hashed_validator;
	}
}

int AuthServerDatabaseHelper::get_user_id(std::string user_name){

	this->_prepare_statement("select user_id from users where user_name = ?");
	this->_bind_statement_text(1, user_name.c_str());

	int execute_status = this->_execute_statement();

	if (execute_status != SQLITE_ROW){
		this->_finalize_statement();
		throw CasaMartinezException(11, execute_status);
	}else{
		// Get user id from table
		int user_id = sqlite3_column_int(_statement, 0);
		this->_finalize_statement();

		return user_id;
	}
}

bool AuthServerDatabaseHelper::selector_in_database(std::string selector, std::string app_name){

	this->_prepare_statement("select user_id from auth_tokens where selector = ? and app_name = ?");
	this->_bind_statement_text(1, selector.c_str());
	this->_bind_statement_text(2, app_name.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	return execute_status == SQLITE_ROW;
}

bool AuthServerDatabaseHelper::auth_token_in_database(int user_id, std::string app_name){

	this->_prepare_statement("select selector from auth_tokens where user_id = ? and app_name = ?");
	this->_bind_statement_int(1, user_id);
	this->_bind_statement_text(2, app_name.c_str());

  	int execute_status = this->_execute_statement();

  	this->_finalize_statement();

	return execute_status == SQLITE_ROW;
}

bool AuthServerDatabaseHelper::user_name_in_database(std::string user_name){

	this->_prepare_statement("select user_name from users where user_name = ?");
	this->_bind_statement_text(1, user_name.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	return execute_status == SQLITE_ROW;
}

std::string AuthServerDatabaseHelper::get_user_hashed_password(std::string user_name){
	// Look for user name in users table
	this->_prepare_statement("select user_pass from users where user_name = ?");
	this->_bind_statement_text(1, user_name.c_str());

	int execute_status = this->_execute_statement();

	if (execute_status != SQLITE_ROW){
		this->_finalize_statement();
		throw CasaMartinezException(11, execute_status);
	}else{
		// Retrieve hashed password from row
		const unsigned char* hashed_password_badtype = sqlite3_column_text(_statement, 0);

		std::string hashed_password(reinterpret_cast<const char*>(hashed_password_badtype));

		this->_finalize_statement();

		return hashed_password;
	}
}

bool AuthServerDatabaseHelper::app_name_in_database(std::string app_name){

	this->_prepare_statement("select app_name from apps where app_name = ?");
	this->_bind_statement_text(1, app_name.c_str());

	int execute_status = this->_execute_statement();

	this->_finalize_statement();

	return execute_status == SQLITE_ROW;
}
