

add_executable(DummyMasterNode src/DummyMasterNode.cpp)
target_link_libraries(DummyMasterNode PUBLIC CasaMartinezShared)

set_target_properties(DummyMasterNode PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)