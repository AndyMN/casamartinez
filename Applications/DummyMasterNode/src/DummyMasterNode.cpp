#include <iostream>
#include <vector>
#include <string>

#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/StreamSocket.h"
#include "CasaMartinezException.h"
#include "toml.hpp"

#include "BaseServerConnection.h"

int main(int argc, char* argv[])
{
	try{
		
		if (argc < 2){
			std::cerr << "USAGE: " << argv[0] << " <CONFIG>" << std::endl;
			return 1;
		}

		auto config_toml = toml::parse(std::string(argv[1]));

		auto encryption_config = toml::find(config_toml, "encryption");
		std::string aes_key_filename = toml::find<std::string>(encryption_config, "aes_key_path");

		auto securityportal_config = toml::find(config_toml, "securityportal");
		int securityportal_port = toml::find<int>(securityportal_config, "port");

		auto dummy_config = toml::find(config_toml, "dummy");
		std::string request = toml::find<std::string>(dummy_config, "request");
		std::string request_data = toml::find<std::string>(dummy_config, "request_data");

		Poco::Net::SocketAddress sa("127.0.0.1", securityportal_port);

		Poco::Net::StreamSocket ss1(sa);
		BaseServerConnection bs1(ss1, aes_key_filename);

		bs1.send_data(request + "\t" + request_data);

		std::string received_data = bs1.receive_data();
		std::vector<std::string> split_data = bs1.read_received_data(received_data);

		for (std::string data : split_data){
			std::cout << data << std::endl;
		}
	}catch(Poco::Exception& exc){
		std::cerr << exc.displayText() << std::endl;
	}catch (CasaMartinezException& cmexc){
		std::cerr << "CasaMartinezException: " << cmexc.get_error_code() << " " <<cmexc.get_error_message() << std::endl;
	}


    return 0;
}
