#include <iostream>
#include <vector>
#include <string>

#include "Poco/Net/SecureSMTPClientSession.h"
#include "Poco/Net/MailMessage.h"
#include "Poco/Net/MailRecipient.h"
#include "toml.hpp"

int main(int argc, char* argv[])
{

	try{

		if (argc < 2){
			std::cerr << "USAGE: " << argv[0] << " <CONFIG>" << std::endl;
			return 1;
		}

		auto config_toml = toml::parse(std::string(argv[1]));

		auto email_config = toml::find(config_toml, "email");	

		std::string email_account = toml::find<std::string>(email_config, "account");
		std::string email_password = toml::find<std::string>(email_config, "password");
		std::string email_smtp_host = toml::find<std::string>(email_config, "smtp_host");
		int email_smtp_port = toml::find<int>(email_config, "smtp_port");

		auto dummy_config = toml::find(config_toml, "dummy");
		std::string to_email_address = toml::find<std::string>(dummy_config, "to_email_address");

		Poco::Net::SecureSMTPClientSession smtp_client(email_smtp_host, email_smtp_port);
		smtp_client.login();
		smtp_client.startTLS();
		smtp_client.login(Poco::Net::SMTPClientSession::AUTH_PLAIN, email_account,  email_password);

		Poco::Net::MailRecipient recipient(Poco::Net::MailRecipient::PRIMARY_RECIPIENT, to_email_address);
		std::vector<Poco::Net::MailRecipient> recipients = {recipient};
		Poco::Net::MailMessage mail_message;
		mail_message.setRecipients(recipients);
		mail_message.setContentType("text/plain; charset=UTF-8");
		mail_message.setContent("I am a testboy.", Poco::Net::MailMessage::ENCODING_8BIT);
		mail_message.setSubject("ALARM");
		smtp_client.sendMessage(mail_message);


	}catch (Poco::Exception& exc){
		std::cerr << "PocoException: " << exc.displayText() << std::endl;
	}




  return 0;
}
