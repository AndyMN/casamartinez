#include <iostream>

#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/SharedPtr.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/ServerApplication.h"

#include "WebServerRequestHandlerFactory.h"
#include "CasaMartinezException.h"
#include "BaseServerConnection.h"
#include "toml.hpp"

class SMSWebServer: public Poco::Util::ServerApplication{

public:
	SMSWebServer(){}
	~SMSWebServer(){}

protected:
	void initialize(Poco::Util::Application& self){
		Poco::Util::Application::loadConfiguration();
		Poco::Util::ServerApplication::initialize(self);
	}

	void uninitialize(){
		Poco::Util::ServerApplication::uninitialize();
	}

	int main(const std::vector<std::string>& args){

		if (args.size() < 1){
			std::cerr << "Usage: <CONFIG LOCATION>"  << std::endl;
			return 1;
		}

		auto config_toml = toml::parse(args[0]);

		auto smswebserver_config = toml::find(config_toml, "smswebserver");
		int smswebserver_port = toml::find<int>(smswebserver_config, "port");

		auto securityportal_config = toml::find(config_toml, "securityportal");
		std::string securityportal_ip = toml::find<std::string>(securityportal_config, "ip");
		int securityportal_port = toml::find<int>(securityportal_config, "port");

		auto encryption_config = toml::find(config_toml, "encryption");
		std::string aes_key_filename = toml::find<std::string>(encryption_config, "aes_key_path");


		WebServerRequestHandlerFactory req_factory(securityportal_ip, securityportal_port, aes_key_filename);

		Poco::Net::HTTPServer http_server(&req_factory, smswebserver_port);
		http_server.start();

		std::cout << "SMSWebServer Started." << std::endl;
		waitForTerminationRequest();
		http_server.stop();
	}
};

int main(int argc, char* argv[])
{
	SMSWebServer myApp;
	return myApp.run(argc, argv);
}
