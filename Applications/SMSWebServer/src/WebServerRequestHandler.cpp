#include <iostream>
#include <string>

#include "WebServerRequestHandler.h"
#include "BaseServerConnection.h"

#include "Poco/StreamCopier.h"
#include "Poco/Net/HTMLForm.h"

WebServerRequestHandler::WebServerRequestHandler(std::string securityportal_ip, int securityportal_port, std::string aes_key_filename)
  :Poco::Net::HTTPRequestHandler(),
   _securityportal_ip(securityportal_ip),
   _securityportal_port(securityportal_port),
   _aes_key_filename(aes_key_filename){
}

WebServerRequestHandler::~WebServerRequestHandler(){
}

void WebServerRequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response){

  Poco::Net::HTMLForm form(request, request.stream());

  // Send correct response to Twilio (doesnt matter what but we dont want an error)
  response.setContentType("text/html");
  std::ostream& ostr = response.send();
  ostr << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  ostr << "<Response>";
  // Empty response will make it so that no sms message is sent back but also no error. If we want to send an sms back use the line below
  //ostr << "  <Message> Thanks for calling! </Message> ";
  ostr << "</Response>";


  // Get number of sender (from)
  std::string from = form["From"];
  std::cout << "From: " << from << std::endl;
  // Get message of sender
  std::string body = form["Body"];
  std::cout << "Body: " << body << std::endl;

  // Make a connection to the security portal
  Poco::Net::SocketAddress securityportal_sa(_securityportal_ip, _securityportal_port);
  Poco::Net::StreamSocket securityportal_ss(securityportal_sa);
  BaseServerConnection securityportal_bs(securityportal_ss, _aes_key_filename);
  // Send "from_number" and "body" to security portal
  // ["SMS"\tFrom\tMessage]
  securityportal_bs.send_data("SMS\t" + from + "\t" + body);


  /*std::string to_country = form["ToCountry"];
  std::string to_state = form["ToState"];
  std::string to_city = form["ToCity"];
  std::string to_zip = form["ToZip"];
  std::string to = form["To"];

  std::string from_country = form["FromCountry"];
  std::string from_state = form["FromState"];
  std::string from_city = form["FromCity"];
  std::string from_zip = form["FromZip"];

  std::string body = form["Body"];

  std::cout << "To Country: " << to_country << std::endl;
  std::cout << "To State: " << to_state << std::endl;
  std::cout << "To City: " << to_city << std::endl;
  std::cout << "To Zip: " << to_zip << std::endl;
  std::cout << "To: " << to << std::endl;

  std::cout << "From Country: " << from_country << std::endl;
  std::cout << "From State: " << from_state << std::endl;
  std::cout << "From City: " << from_city << std::endl;
  std::cout << "From Zip: " << from_zip << std::endl;*/
}
