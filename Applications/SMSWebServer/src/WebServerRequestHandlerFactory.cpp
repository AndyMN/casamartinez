#include <iostream>

#include "WebServerRequestHandlerFactory.h"


WebServerRequestHandlerFactory::WebServerRequestHandlerFactory(std::string securityportal_ip, int securityportal_port, std::string aes_key_filename)
  :Poco::Net::HTTPRequestHandlerFactory(),
   _securityportal_ip(securityportal_ip),
   _securityportal_port(securityportal_port),
   _aes_key_filename(aes_key_filename){
}

WebServerRequestHandlerFactory::~WebServerRequestHandlerFactory(){
}

WebServerRequestHandler* WebServerRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request){

  WebServerRequestHandler* request_handler = new WebServerRequestHandler(_securityportal_ip, _securityportal_port, _aes_key_filename);

  return request_handler;
}
