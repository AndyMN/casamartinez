#ifndef WEBSERVERREQUESTHANDLERFACTORY_H
#define WEBSERVERREQUESTHANDLERFACTORY_H

#include <string>

#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "WebServerRequestHandler.h"

class WebServerRequestHandlerFactory: public Poco::Net::HTTPRequestHandlerFactory
{
public:
  WebServerRequestHandlerFactory(std::string securityportal_ip, int securityportal_port, std::string aes_key_filename);
  ~WebServerRequestHandlerFactory();

  WebServerRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);

private:

  std::string _securityportal_ip;
  int _securityportal_port;
  std::string _aes_key_filename;
};



#endif
