#ifndef WEBSERVERREQUESTHANDLER_H
#define WEBSERVERREQUESTHANDLER_H

#include <string>

#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"

class WebServerRequestHandler: public Poco::Net::HTTPRequestHandler
{
public:

  WebServerRequestHandler(std::string securityportal_ip, int securityportal_port, std::string aes_key_filename);
  ~WebServerRequestHandler();

  void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);

private:
  std::string _securityportal_ip;
  int _securityportal_port;
  std::string _aes_key_filename;
};



#endif
