#include <iostream>
#include <vector>
#include <string>

#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/TCPServer.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/ServerApplication.h"


#include "SecurityPortalConnection.h"
#include "SecurityPortalConnectionFactory.h"
#include "SecurityPortalSMSManager.h"
#include "SecurityPortalMailManager.h"
#include "toml.hpp"


class SecurityPortal: public Poco::Util::ServerApplication{

public:
	SecurityPortal(){}
	~SecurityPortal(){}

protected:
	void initialize(Poco::Util::Application& self){
		Poco::Util::Application::loadConfiguration();
		Poco::Util::ServerApplication::initialize(self);
	}

	void uninitialize(){
		Poco::Util::ServerApplication::uninitialize();
	}

	int main(const std::vector<std::string>& args){

		if (args.size() < 1){
			std::cerr << "Usage: <CONFIG LOCATION>"  << std::endl;
			return 1;
		}

		auto config_toml = toml::parse(args[0]);

		auto authserver_config = toml::find(config_toml, "authserver");
		std::string authserver_ip = toml::find<std::string>(authserver_config, "ip");
		int authserver_port = toml::find<int>(authserver_config, "port");
		
		auto database_config = toml::find(config_toml, "database");
		std::string database_location = toml::find<std::string>(database_config, "path");
		
		auto encryption_config = toml::find(config_toml, "encryption");
		std::string aes_key_filename = toml::find<std::string>(encryption_config, "aes_key_path");

		auto securityportal_config = toml::find(config_toml, "securityportal");
		int securityportal_port = toml::find<int>(securityportal_config, "port");

		auto twilio_config = toml::find(config_toml, "twilio");
		std::string twilio_account_sid = toml::find<std::string>(twilio_config, "account_sid");
		std::string twilio_auth_token = toml::find<std::string>(twilio_config, "auth_token");
		std::string twilio_from_number = toml::find<std::string>(twilio_config, "phone_number");

		auto email_config = toml::find(config_toml, "email");	

		std::string email_account = toml::find<std::string>(email_config, "account");
		std::string email_password = toml::find<std::string>(email_config, "password");
		std::string email_smtp_host = toml::find<std::string>(email_config, "smtp_host");
		int email_smtp_port = toml::find<int>(email_config, "smtp_port");

		std::cout << "AuthServer IP: " << authserver_ip << std::endl;
		std::cout << "AuthServer Port: " << authserver_port << std::endl;

		SecurityPortalSMSManager sms_manager(twilio_account_sid, twilio_auth_token, twilio_from_number);
		SecurityPortalMailManager mail_manager(email_smtp_host, email_smtp_port, email_account, email_password);
		SecurityPortalNotifier notifier;
		notifier.set_sms_manager(&sms_manager);
		notifier.set_mail_manager(&mail_manager);

		Poco::Net::SocketAddress authserver_sa(authserver_ip, authserver_port);

		Poco::Net::ServerSocket ss(securityportal_port);
		Poco::Net::TCPServer srv(new SecurityPortalConnectionFactory<SecurityPortalConnection>(aes_key_filename, database_location, authserver_sa, &notifier), ss);
		srv.start();
		std::cout << "Security Portal Started." << std::endl;
		waitForTerminationRequest();
		srv.stop();
	}
};

int main(int argc, char* argv[])
{
	SecurityPortal myApp;
	return myApp.run(argc, argv);
}
