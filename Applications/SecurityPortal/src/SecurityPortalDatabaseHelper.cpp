#include <iostream>
#include <cstring>

#include "SecurityPortalDatabaseHelper.h"
#include "DatabaseHelper.h"
#include "CasaMartinezException.h"


SecurityPortalDatabaseHelper::SecurityPortalDatabaseHelper(std::string database_location): DatabaseHelper(database_location){
}

SecurityPortalDatabaseHelper::~SecurityPortalDatabaseHelper(){
}



std::string SecurityPortalDatabaseHelper::get_location_of_sensor(std::string rx_address){

  _prepare_statement("select location from sensor_locations where rx_addr = ?");
  _bind_statement_text(1, rx_address.c_str());

  int execute_status = _execute_statement();


  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(27, execute_status);
  }else{
    const unsigned char* location_of_sensor_badtype = sqlite3_column_text(_statement, 0);
    std::string location_of_sensor(reinterpret_cast<const char*>(location_of_sensor_badtype));

    _finalize_statement();

    return location_of_sensor;
  }
}	

void SecurityPortalDatabaseHelper::add_entry_sensor_states(std::string rx_address, std::string location, int sensor_state){

  _prepare_statement("insert into sensor_states (rx_addr, location, state) values (?, ?, ?)");
  _bind_statement_text(1, rx_address.c_str());
  _bind_statement_text(2, location.c_str());
  _bind_statement_int(3, sensor_state);

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(28, execute_status);
  }
}

std::vector<std::string> SecurityPortalDatabaseHelper::get_sensor_locations(){

    _prepare_statement("select location from sensor_locations");

    int execute_status = _execute_statement();

    if (execute_status != SQLITE_ROW){
      _finalize_statement();
      throw CasaMartinezException(39, execute_status);
    }else{

      std::vector<std::string> sensor_locations;

      while (execute_status == SQLITE_ROW){

        const unsigned char* sensor_location_badtype = sqlite3_column_text(_statement, 0);
        std::string sensor_location(reinterpret_cast<const char*>(sensor_location_badtype));
        std::cout << sensor_location << std::endl;
        sensor_locations.push_back(sensor_location);

        execute_status = sqlite3_step(_statement);
      }
      _finalize_statement();

      return sensor_locations;
    }
}

std::pair<int, int> SecurityPortalDatabaseHelper::get_latest_entry_of_location(std::string location){

  _prepare_statement("select state, creation_time_unix from sensor_states where location = ? order by creation_time_unix desc limit 1");
  _bind_statement_text(1, location.c_str());

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();

    int sensor_state = 2; // This way we can still display the sensor, but it does not have a registered open/closed state yet.
    int creation_time_unix = 0;

    return std::pair<int, int>(sensor_state, creation_time_unix);
  }else{
    int sensor_state = sqlite3_column_int(_statement, 0);
    int creation_time_unix = sqlite3_column_int(_statement, 1);

    _finalize_statement();

    return std::pair<int, int>(sensor_state, creation_time_unix);
  }
}

bool SecurityPortalDatabaseHelper::location_in_database(std::string location){

  _prepare_statement("select * from sensor_locations where location = ?");
  _bind_statement_text(1, location.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status == SQLITE_ROW){
    return true;
  }else{
    return false;
  }
}

void SecurityPortalDatabaseHelper::change_location(std::string old_location, std::string new_location){

  _prepare_statement("update sensor_locations set location = ? where location = ?");
  _bind_statement_text(1, new_location.c_str());
  _bind_statement_text(2, old_location.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(45, execute_status);
  }
}



bool SecurityPortalDatabaseHelper::tel_number_in_database(std::string tel_number){

  _prepare_statement("select * from contact_info where tel_nr = ?");
  _bind_statement_text(1, tel_number.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status == SQLITE_ROW){
    return true;
  }else{
    return false;
  }
}

void SecurityPortalDatabaseHelper::change_tel_number(std::string old_tel_number, std::string new_tel_number){

  _prepare_statement("update contact_info set tel_nr = ? where tel_nr = ?");
  _bind_statement_text(1, new_tel_number.c_str());
  _bind_statement_text(2, old_tel_number.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(40, execute_status);
  }
}

bool SecurityPortalDatabaseHelper::email_in_database(std::string email){
  _prepare_statement("select * from contact_info where email = ?");
  _bind_statement_text(1, email.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status == SQLITE_ROW){
    return true;
  }else{
    return false;
  }
}

void SecurityPortalDatabaseHelper::change_email(std::string old_email, std::string new_email){

  _prepare_statement("update contact_info set email = ? where email = ?");
  _bind_statement_text(1, new_email.c_str());
  _bind_statement_text(2, old_email.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(41, execute_status);
  }
}

bool SecurityPortalDatabaseHelper::name_in_database(std::string name){

  _prepare_statement("select * from contact_info where name = ?");
  _bind_statement_text(1, name.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status == SQLITE_ROW){
    return true;
  }else{
    return false;
  }
}

void SecurityPortalDatabaseHelper::add_contact(std::string name, std::string tel_number, std::string email){

  _prepare_statement("insert into contact_info (name, tel_nr, email) values (?, ?, ?)");
  _bind_statement_text(1, name.c_str());
  _bind_statement_text(2, tel_number.c_str());
  _bind_statement_text(3, email.c_str());

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(51, execute_status);
  }
}



bool SecurityPortalDatabaseHelper::alarm_is_on(){

  _prepare_statement("select state from alarm_states order by creation_time_unix desc limit 1");

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(29, execute_status);
  }else{
    int state = sqlite3_column_int(_statement, 0);
    _finalize_statement();

    if (state == 1){
      return true;
    }else if (state == 0){
      return false;
    }else{
      throw CasaMartinezException(30);
    }
  }
}

void SecurityPortalDatabaseHelper::add_entry_alarm_state(int alarm_state){

  _prepare_statement("insert into alarm_states (state) values (?)");
  _bind_statement_int(1, alarm_state);

  int execute_status = _execute_statement();

  _finalize_statement();

  if (execute_status != SQLITE_DONE){
    throw CasaMartinezException(42, execute_status);
  }
}


std::vector<std::string> SecurityPortalDatabaseHelper::get_names(){

  _prepare_statement("select name from contact_info");

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(43, execute_status);
  }else{

    std::vector<std::string> names;

    while (execute_status == SQLITE_ROW){

      const unsigned char* name_badtype = sqlite3_column_text(_statement, 0);
      std::string name(reinterpret_cast<const char*>(name_badtype));
      names.push_back(name);

      execute_status = sqlite3_step(_statement);
    }
    _finalize_statement();

    return names;
  }
}


std::vector<std::string> SecurityPortalDatabaseHelper::get_email_addresses(){
  _prepare_statement("select email from contact_info");

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(31, execute_status);
  }else{

    std::vector<std::string> email_addresses;

    while (execute_status == SQLITE_ROW){

      const unsigned char* email_address_badtype = sqlite3_column_text(_statement, 0);
      std::string email_address(reinterpret_cast<const char*>(email_address_badtype));
      email_addresses.push_back(email_address);

      execute_status = sqlite3_step(_statement);
    }
    _finalize_statement();

    return email_addresses;
  }
}



std::vector<std::string> SecurityPortalDatabaseHelper::get_tel_numbers(){

  _prepare_statement("select tel_nr from contact_info");

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(32, execute_status);
  }else{

    std::vector<std::string> tel_numbers;

    while (execute_status == SQLITE_ROW){

      const unsigned char* tel_number_badtype = sqlite3_column_text(_statement, 0);
      std::string tel_number(reinterpret_cast<const char*>(tel_number_badtype));
      tel_numbers.push_back(tel_number);

      execute_status = sqlite3_step(_statement);
    }
    _finalize_statement();

    return tel_numbers;
  }
}

std::vector<std::vector<std::string>> SecurityPortalDatabaseHelper::get_contact_info(){

  _prepare_statement("select name, tel_nr, email from contact_info");

  int execute_status = _execute_statement();

  if (execute_status != SQLITE_ROW){
    _finalize_statement();
    throw CasaMartinezException(43, execute_status);
  }else{

    std::vector<std::vector<std::string>> contacts;

    while (execute_status == SQLITE_ROW){

      const unsigned char* name_badtype = sqlite3_column_text(_statement, 0);
      std::string name(reinterpret_cast<const char*>(name_badtype));

      const unsigned char* tel_number_badtype = sqlite3_column_text(_statement, 1);
      std::string tel_number(reinterpret_cast<const char*>(tel_number_badtype));

      const unsigned char* email_badtype = sqlite3_column_text(_statement, 2);
      std::string email(reinterpret_cast<const char*>(email_badtype));

      std::vector<std::string> contact = {name, tel_number, email};

      contacts.push_back(contact);

      execute_status = sqlite3_step(_statement);
    }
    _finalize_statement();

    return contacts;
  }
}
