#include <iostream>
#include <string_view>
#include <algorithm>


#include "BaseServerConnection.h"
#include "DatabaseHelper.h"
#include "CasaMartinezException.h"
#include "CasaMartinezLogger.h"

#include "SecurityPortalConnection.h"
#include "SecurityPortalNotifier.h"

SecurityPortalConnection::SecurityPortalConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename, std::string database_location, Poco::Net::SocketAddress authserver_sa, SecurityPortalNotifier* notifier)
	:BaseServerConnection(s, aes_key_filename),
	 _dh(database_location),
	 _authserver_sa(authserver_sa),
	 _notifier(notifier), 
	 _aes_key_filename(aes_key_filename),
	 _cl("SecurityPortalLog.txt", "SecurityPortalErrorLog.txt"){}

SecurityPortalConnection::~SecurityPortalConnection(){}

void SecurityPortalConnection::handle_local_connection(std::vector<std::string>& split_data){
	try{
		if (split_data[0] == "RX"){
			// Sensor state change   ["RX"\tRX_address\tstate]
			std::string rx_address = split_data[1];

			// A valid RX address has 5 bytes written as 10 nibbles
			if (rx_address.size() != 10){
				// Invalid rx address length. Throw
				throw CasaMartinezException(25);
			}

			std::string sensor_state_str = split_data[2];

			if (sensor_state_str != "0" && sensor_state_str != "1"){
				// Invalid sensor state. Throw
				throw CasaMartinezException(26);
			}

			// A valid sensor state is 0 or 1
			int sensor_state = std::stoi(sensor_state_str);
			
			bool alarm_is_on = _dh.alarm_is_on();

			if (alarm_is_on){
				// If the alarm is on send notifications around to users:
				std::cout << "Alarm is on" << std::endl;

				// TEXT MESSAGE
				_notifier->send_sms_to_all("Sensor state changed", _incoming_connection_address, &_cl, &_dh);

				// WHATS APP MESSAGE
				//_notifier->send_WA_to_all("Sensor state changed", _incoming_connection_address, &_cl, &_dh);

				// PUSH NOTIFICATION
				_notifier->send_push_to_all("Sensor state changed", _incoming_connection_address, &_cl, &_dh);

				// EMAIL
				_notifier->send_email_to_all("Sensor state changed", "ALAAAAARM", _incoming_connection_address, &_cl, &_dh);
			}

			// Add entry of new sensor state change to table
			// Look in table sensor_locations for the current location of sensor with rx_address
			std::string sensor_location = _dh.get_location_of_sensor(rx_address);
			std::cout << "Sensor: " << rx_address << " at Location: " << sensor_location << std::endl;

			// Add an entry to table sensor_states with [(id), RX_address, Location, State, (Date)]
			_dh.add_entry_sensor_states(rx_address, sensor_location, sensor_state);
		}else if (split_data[0] == "SMS"){
			// USED BY SMS WEBSERVER
			// SMS received ["SMS"\tFrom\tMessage]

			// Check if "From" number is in table contact_info
			std::string from_number = split_data[1];

			if (!_dh.tel_number_in_database(from_number)){
				// Phone number not in contact info. Throw
				throw CasaMartinezException(37);
			}

			std::string message = split_data[2];
			// Change the message to lowercase so that "ON" == "on" == "On" etc.
			std::transform(message.begin(), message.end(), message.begin(), ::tolower);

			int alarm_state = 2;
			if (message == "on" || message == "aan" || message == "1"){
				alarm_state = 1;
			}else if (message == "off" || message == "af" || message == "uit" || message == "0"){
				alarm_state = 0;
			}else{
				// Invalid message. Throw
				throw CasaMartinezException(52);
			}

			// Create new entry in alarm_states
			_dh.add_entry_alarm_state(alarm_state);
			// Check if alarm is on or off
			bool current_alarm_state = _dh.alarm_is_on();

			// Notify users that alarm state has changed (SMS, Email, WA, Push)

			// TEXT MESSAGE
			_notifier->send_sms_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// WHATS APP MESSAGE
			//_notifier->send_WA_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// PUSH NOTIFICATION
			_notifier->send_push_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// EMAIL
			_notifier->send_email_to_all("Alarm: " + std::to_string(current_alarm_state), "ALARM CHANGED", _incoming_connection_address, &_cl, &_dh);
		}else{
			// Not a valid master node request. Throw
			throw CasaMartinezException(55);
		}
	}catch (Poco::Exception& exc){
		std::cerr << _incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << std::endl;
		this->send_data("ERROR\tPE");
		_cl.log_error(_incoming_connection_address + "\t" + exc.displayText());
	}catch (CasaMartinezException& cmexc){
		std::cerr << _incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << std::endl;
		this->send_data("ERROR\t" + std::to_string(cmexc.get_error_code()));
		_cl.log_error(_incoming_connection_address + "\t" + cmexc.get_error_message());
	}catch (std::exception& e){
		std::cerr << e.what() << std::endl;
		this->send_data("ERROR\tCPP");
		_cl.log_error(_incoming_connection_address + "\t" + e.what());
	}
}

void SecurityPortalConnection::handle_nonlocal_connection(std::vector<std::string>& split_data){
	
	try{
		if (split_data.size() <= 4){
			throw CasaMartinezException(22);
		}

		std::string& auth_method = split_data[0];
		std::string& user_name = split_data[1];
		std::string& user_credential = split_data[2];
		std::string& app_name = split_data[3];

		Poco::Net::StreamSocket auth_ss(_authserver_sa);
		BaseServerConnection auth_bs(auth_ss, _aes_key_filename);
		auth_bs.send_data(auth_method + "\t" + user_name + "\t" + user_credential + "\t" + app_name);
		std::string received_data_auth = auth_bs.receive_data();
		std::vector<std::string> split_data_auth = auth_bs.read_received_data(received_data_auth);

		if (split_data_auth[0] == "ERROR"){
			throw CasaMartinezException(std::stoi(split_data_auth[1]));
		}else if (split_data_auth[0] != "OK"){
			throw CasaMartinezException(21);
		}

		std::string& selector = split_data_auth[2];
		std::string& validator = split_data_auth[3];
		
		std::string& request = split_data[4];

		if (std::find(_valid_requests.begin(), _valid_requests.end(), request) == _valid_requests.end()){
			// Request not recognized. Throw
			throw CasaMartinezException(23);
		}

		if (request == "state"){
			//[selectvalid, selector, validator, app_name, "state", combination]
			std::string state_data = "";
  //void send_WA_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh);

			// Get all locations from sensor_locations
			std::vector<std::string> sensor_locations = _dh.get_sensor_locations();
			// For every location get the latest entry in sensor_states
			for (std::string location: sensor_locations){
				std::pair<int, int> latest_entry_of_location = _dh.get_latest_entry_of_location(location);
				int sensor_state = latest_entry_of_location.first;
				int creation_time_unix = latest_entry_of_location.second;

				// Store state and date of latest entry
				state_data += "\t" + location + "\t" + std::to_string(sensor_state) + "\t" + std::to_string(creation_time_unix);
			}

			// Send Location \t State \t Date for every location
			this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + state_data);
		}else if (request == "alarm"){
			//[selectvalid, selector, validator, app_name, "alarm", "alarm state (0 or 1)" combination]

			if (split_data.size() <= 5){
				// No alarm state present
				throw CasaMartinezException(33);
			}

			int alarm_state = std::stoi(split_data[5]);

			if (alarm_state != 1 || alarm_state != 0){
				// Invalid alarm state
				throw CasaMartinezException(34);
			}

			// Create new entry in alarm_states
			_dh.add_entry_alarm_state(alarm_state);
			// Check if alarm is on or off
			bool current_alarm_state = _dh.alarm_is_on();
			// Send alarm state back to user
			this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + "\t" + std::to_string(current_alarm_state));

			// Notify users that alarm state has changed (SMS, Email, WA, Push)

			// TEXT MESSAGE
			_notifier->send_sms_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// WHATS APP MESSAGE
			//_notifier->send_WA_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// PUSH NOTIFICATION
			_notifier->send_push_to_all("Alarm: " + std::to_string(current_alarm_state), _incoming_connection_address, &_cl, &_dh);

			// EMAIL
			_notifier->send_email_to_all("Alarm: " + std::to_string(current_alarm_state), "ALARM CHANGED", _incoming_connection_address, &_cl, &_dh);
		}else if (request == "changecontact"){
			//[selectvalid, selector, validator, app_name, "changecontact", "specifier (email or telnr)", "old", "new", combination]

			if (split_data.size() <= 7){
				// No specifiers
				throw CasaMartinezException(35);
			}

			std::string specifier = split_data[5];
			
			if (specifier != "telnr" || specifier != "email"){
				// Invalid specifier
				throw CasaMartinezException(36);
			}

			if (specifier == "telnr"){
				std::string old_telnr = split_data[6];
				std::string new_telnr = split_data[7];

				// Check if old_telnr in contact_info
				bool tel_number_in_database = _dh.tel_number_in_database(old_telnr);

				if (!tel_number_in_database){
					// Old tel nr not in database
					throw CasaMartinezException(37);
				}

				// Change old_telnr to new_telnr
				_dh.change_tel_number(old_telnr, new_telnr);
				// Read every entry of contact_info
				std::vector<std::vector<std::string>> contact_info = _dh.get_contact_info();
				std::string changecontact_data = "";
				// Send every entry as Name \t telnr \t email
				for (std::vector<std::string> contact: contact_info){
					if (contact.size() == 3){
						std::string name = contact[0];
						std::string telnr = contact[1];
						std::string email = contact[2];
						changecontact_data += "\t" + name + "\t" + telnr + "\t" + email;
					}
				}

				this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + changecontact_data);
			}else if (specifier == "email"){
				std::string old_email = split_data[6];
				std::string new_email = split_data[7];

				// Check if old_email in contact_info
				bool email_in_database = _dh.email_in_database(old_email);

				if (!email_in_database){
					// Old email not in database
					throw CasaMartinezException(38);
				}

				// Change old_email to new_email
				_dh.change_email(old_email, new_email);
				// Read every entry of contact_info
				std::vector<std::vector<std::string>> contact_info = _dh.get_contact_info();
				std::string changecontact_data = "";
				// Send every entry as Name \t telnr \t email
				for (std::vector<std::string> contact: contact_info){
					if (contact.size() == 3){
						std::string name = contact[0];
						std::string telnr = contact[1];
						std::string email = contact[2];
						changecontact_data += "\t" + name + "\t" + telnr + "\t" + email;
					}
				}

				this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + changecontact_data);
			}
		}else if (request == "changelocation"){
			//[selectvalid, selector, validator, app_name, "changelocation", "old", "new", combination]

			if (split_data.size() <= 6){
				// No old/new location data present
				throw CasaMartinezException(44);
			}

			std::string old_location = split_data[5];
			std::string new_location = split_data[6];

			// Check if old_location in sensor_locations
			bool location_in_database = _dh.location_in_database(old_location);

			if (!location_in_database){
				// Location not in databse
				throw CasaMartinezException(46);
			}

			std::string changelocation_data = "";
			// If it is change old_location to new_location
			_dh.change_location(old_location, new_location);
			// Get all locations from sensor_locations
			std::vector<std::string> sensor_locations = _dh.get_sensor_locations();
			// For every location get the latest entry in sensor_states
			for (std::string location: sensor_locations){
				std::pair<int, int> latest_entry_of_location = _dh.get_latest_entry_of_location(location);
				int sensor_state = latest_entry_of_location.first;
				int creation_time_unix = latest_entry_of_location.second;

				// Store state and date of latest entry
				changelocation_data += "\t" + location + "\t" + std::to_string(sensor_state) + "\t" + std::to_string(creation_time_unix);
			}

			this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + changelocation_data);
		}else if (request == "contacts"){
			//[selectvalid, selector, validator, app_name, "contacts", combination]

			// Read every entry of contact_info
			std::vector<std::vector<std::string>> contact_info = _dh.get_contact_info();
			std::string contacts_data = "";
			// Send every entry as Name \t telnr \t email
			for (std::vector<std::string>& contact: contact_info){
				if (contact.size() == 3){
					std::string& name = contact[0];
					std::string& telnr = contact[1];
					std::string& email = contact[2];
					contacts_data += "\t" + name + "\t" + telnr + "\t" + email;
				}
			}

			this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + contacts_data);
		}else if (request == "addcontact"){
			//[selectvalid, selector, validator, app_name, "addcontact", "name", "telnr", "email", combination]

			if (split_data.size() <= 7){
				// Not enough contact info
				throw CasaMartinezException(47);
			}

			std::string name = split_data[5];
			std::string tel_number = split_data[6];
			std::string email = split_data[7];

			// Check if name OR telnr OR email is in contact_info
			bool name_in_database = _dh.name_in_database(name);
			bool tel_number_in_database = _dh.tel_number_in_database(tel_number);
			bool email_in_database = _dh.email_in_database(email);

			if (name_in_database){
				// Name already in database
				throw CasaMartinezException(50);
			}

			if (tel_number_in_database){
				// Tel number already in database
				throw CasaMartinezException(49);
			}

			if (email_in_database){
				// Email already in database
				throw CasaMartinezException(48);
			}

			std::string addcontact_data = "";

			// Add contact to contact_info
			_dh.add_contact(name, tel_number, email);
			// Read every entry of contact_info
			std::vector<std::vector<std::string>> contact_info = _dh.get_contact_info();
			std::string contacts_data = "";
			// Send every entry as Name \t telnr \t email
			for (std::vector<std::string> contact: contact_info){
				if (contact.size() == 3){
					std::string name = contact[0];
					std::string telnr = contact[1];
					std::string email = contact[2];
					addcontact_data += "\t" + name + "\t" + telnr + "\t" + email;
				}
			}

			this->send_data("OK\tselectvalid\t" + selector + "\t" + validator + addcontact_data);
		}
	}catch (Poco::Exception& exc){
		std::cerr << _incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << std::endl;
		this->send_data("ERROR\tPE");
		_cl.log_error(_incoming_connection_address + "\t" + exc.displayText());
	}catch (CasaMartinezException& cmexc){
		std::cerr << _incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << std::endl;
		this->send_data("ERROR\t" + std::to_string(cmexc.get_error_code()));
		_cl.log_error(_incoming_connection_address + "\t" + cmexc.get_error_message());
	}catch (std::exception& e){
		std::cerr << e.what() << std::endl;
		this->send_data("ERROR\tCPP");
		_cl.log_error(_incoming_connection_address + "\t" + e.what());
	}
}


void SecurityPortalConnection::run(){

	_incoming_connection_address = this->socket().peerAddress().host().toString();

	std::cout << "Connection from: " << _incoming_connection_address << std::endl;

	bool connection_is_local = _incoming_connection_address == "127.0.0.1";

	_cl.log(_incoming_connection_address);

	try{
		std::string received_data = receive_data();

		std::vector<std::string> split_data = read_received_data(received_data);

		for (std::string s: split_data){
			std::cout << s << std::endl;
		}

		if (connection_is_local){
			this->handle_local_connection(split_data);			
		}else{
			this->handle_nonlocal_connection(split_data);
		}
	}catch (Poco::Exception& exc){
		std::cerr << _incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << std::endl;
		this->send_data("ERROR\tPE");
		_cl.log_error(_incoming_connection_address + "\t" + exc.displayText());
	}catch (CasaMartinezException& cmexc){
		std::cerr << _incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << std::endl;
		this->send_data("ERROR\t" + std::to_string(cmexc.get_error_code()));
		_cl.log_error(_incoming_connection_address + "\t" + cmexc.get_error_message());
	}catch (std::exception& e){
		std::cerr << e.what() << std::endl;
		this->send_data("ERROR\tCPP");
		_cl.log_error(_incoming_connection_address + "\t" + e.what());
	}
}
