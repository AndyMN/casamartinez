#include <iostream>

#include "SecurityPortalSMSManager.h"

#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPBasicCredentials.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/URI.h"


SecurityPortalSMSManager::SecurityPortalSMSManager(std::string account_sid, std::string auth_token, std::string from_tel_number)
	:_account_sid(account_sid), 
	 _auth_token(auth_token),
	 _from_tel_number(from_tel_number){
}

SecurityPortalSMSManager::~SecurityPortalSMSManager(){
}


void SecurityPortalSMSManager::send_sms(std::string to_tel_number, std::string message){

	Poco::URI uri("https://api.twilio.com/2010-04-01/Accounts/" + _account_sid + "/Messages.json");

	Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());

	Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, uri.getPath(), Poco::Net::HTTPMessage::HTTP_1_1);

	Poco::Net::HTTPBasicCredentials cred(_account_sid, _auth_token);
	cred.authenticate(req);

	Poco::Net::HTMLForm form;
	form.add("From", _from_tel_number);
	form.add("To", to_tel_number);
	form.add("Body", message);
	form.prepareSubmit(req);

	std::ostream& stream = session.sendRequest(req);
	form.write(stream);

	Poco::Net::HTTPResponse response;
	session.receiveResponse(response);
}
