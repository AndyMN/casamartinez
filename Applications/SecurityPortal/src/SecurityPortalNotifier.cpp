
#include "BaseServerConnection.h"
#include "SecurityPortalNotifier.h"
#include "CasaMartinezException.h"

SecurityPortalNotifier::SecurityPortalNotifier(){
}

SecurityPortalNotifier::~SecurityPortalNotifier(){
}

void SecurityPortalNotifier::set_sms_manager(SecurityPortalSMSManager* sms_manager){
  _sms_manager = sms_manager;
}

void SecurityPortalNotifier::set_mail_manager(SecurityPortalMailManager* mail_manager){
  _mail_manager = mail_manager;
}

void SecurityPortalNotifier::send_sms_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh){

  try{
    // Check tel nr in table contact_info
    std::vector<std::string> tel_numbers = dh->get_tel_numbers();
    for (std::string tel_nr: tel_numbers){
      // Send a text message to every nr
      _sms_manager->send_sms(tel_nr, message);
    }

  }catch (CasaMartinezException& cmexc){
    std::cerr << incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << "\tSMS" << std::endl;
    cl->log_error(incoming_connection_address + "\t" + cmexc.get_error_message() + "\tSMS");
  }catch (Poco::Exception& exc){
		std::cerr << incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << "\tSMS" << std::endl;
		cl->log_error(incoming_connection_address + "\t" + exc.displayText() + "\tSMS");
  }
}

void SecurityPortalNotifier::send_email_to_all(std::string message, std::string subject, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh){

  try{
    // Check email addresses in table contact_info
    std::vector<std::string> email_addresses = dh->get_email_addresses();
    for (std::string email_adr: email_addresses){
      // Send an email to every email address found
      _mail_manager->send_mail(message, subject, email_adr);
      //std::cout << email_adr << std::endl;
    }

  }catch (CasaMartinezException& cmexc){
    std::cerr << incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << "\tEmail" << std::endl;
    cl->log_error(incoming_connection_address + "\t" + cmexc.get_error_message() + "\tEmail");
  }catch (Poco::Exception& exc){
		std::cerr << incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << "\tEmail" << std::endl;
		cl->log_error(incoming_connection_address + "\t" + exc.displayText() + "\tEmail");
  }
}

/*
void SecurityPortalNotifier::send_WA_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh){

  try{
    // Check tel nr in table contact_info
    std::vector<std::string> tel_numbers = dh->get_tel_numbers();
    for (std::string tel_nr: tel_numbers){
      // Send a Whats App message to every nr
      //_sms_manager->send_WA(tel_nr, message);
      //std::cout << tel_nr << std::endl;
    }

  }catch (CasaMartinezException& cmexc){
    std::cerr << incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << "\tWA" << std::endl;
    cl->log_error(incoming_connection_address + "\t" + cmexc.get_error_message() + "\tWA");
  }catch (Poco::Exception& exc){
		std::cerr << incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << "\tWA" << std::endl;
		cl->log_error(incoming_connection_address + "\t" + exc.displayText() + "\tWA");
  }
}*/

void SecurityPortalNotifier::send_push_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh){
  try{
    // Send a push notification to every user

  }catch (CasaMartinezException& cmexc){
    std::cerr << incoming_connection_address + "\t" + "CasaMartinezException: " << cmexc.get_error_message() << "\tPush" << std::endl;
    cl->log_error(incoming_connection_address + "\t" + cmexc.get_error_message() + "\tPush");
  }catch (Poco::Exception& exc){
		std::cerr << incoming_connection_address + "\t" + "PocoException: " << exc.displayText() << "\tPush" << std::endl;
		cl->log_error(incoming_connection_address + "\t" + exc.displayText() + "\tPush");
  }
}
