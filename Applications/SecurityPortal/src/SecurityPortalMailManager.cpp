#include <vector>
#include <iostream>

#include "SecurityPortalMailManager.h"

#include "Poco/Net/MailMessage.h"
#include "Poco/Net/MailRecipient.h"



SecurityPortalMailManager::SecurityPortalMailManager(std::string host, int port, std::string account, std::string password): _smtp_client(host, port){

try{
  _smtp_client.login();
  _smtp_client.startTLS();
  _smtp_client.login(Poco::Net::SMTPClientSession::AUTH_PLAIN, account, password);
  }catch (Poco::Exception& exc){
		std::cerr << "PocoException: " << exc.displayText() << std::endl;
	}
}

SecurityPortalMailManager::~SecurityPortalMailManager(){
}

void SecurityPortalMailManager::send_mail(std::string message, std::string subject, std::string recipient){

  Poco::Net::MailRecipient mail_recipient(Poco::Net::MailRecipient::PRIMARY_RECIPIENT, recipient);
  std::vector<Poco::Net::MailRecipient> mail_recipients = {mail_recipient};
  Poco::Net::MailMessage mail_message;
  mail_message.setRecipients(mail_recipients);
  mail_message.setContentType("text/plain; charset=UTF-8");
  mail_message.setContent(message, Poco::Net::MailMessage::ENCODING_8BIT);
  mail_message.setSubject(subject);

  _smtp_client.sendMessage(mail_message);
}
