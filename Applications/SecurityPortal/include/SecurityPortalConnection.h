#ifndef SECURITYPORTALCONNECTION_H
#define SECURITYPORTALCONNECTION_H


#include "BaseServerConnection.h"
#include "SecurityPortalDatabaseHelper.h"
#include "SecurityPortalNotifier.h"
#include "CasaMartinezLogger.h"

class SecurityPortalConnection:public BaseServerConnection
{
public:
	SecurityPortalConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename, std::string database_location, Poco::Net::SocketAddress authserver_sa, SecurityPortalNotifier* notifier);
	~SecurityPortalConnection();

	void run();

protected:
	void handle_local_connection(std::vector<std::string>& split_received_data);
	void handle_nonlocal_connection(std::vector<std::string>& split_received_data);

private:
	CasaMartinezLogger _cl;
	SecurityPortalDatabaseHelper _dh;
	Poco::Net::SocketAddress _authserver_sa;
	SecurityPortalNotifier* _notifier;
	std::string _aes_key_filename;
	std::string _incoming_connection_address;
	std::vector<std::string> _valid_requests = {"state", "alarm", "changecontact", "changelocation", "contacts", "addcontact"};

};
#endif // SECURITYPORTALCONNECTION_H
