#ifndef SECURITYPORTALMAILMANAGER_H
#define SECURITYPORTALMAILMANAGER_H

#include <string>
#include "Poco/Net/SecureSMTPClientSession.h"



class SecurityPortalMailManager{

public:
  SecurityPortalMailManager(std::string host, int port, std::string account, std::string password);
  ~SecurityPortalMailManager();


  void send_mail(std::string message, std::string subject, std::string recipient);

private:

  Poco::Net::SecureSMTPClientSession _smtp_client;


};
#endif
