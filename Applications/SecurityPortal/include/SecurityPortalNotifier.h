#ifndef SECURITYPORTALNOTIFIER_H
#define SECURITYPORTALNOTIFIER_H

#include "SecurityPortalSMSManager.h"
#include "SecurityPortalMailManager.h"
#include "CasaMartinezLogger.h"
#include "SecurityPortalDatabaseHelper.h"

class SecurityPortalNotifier{

public:
  SecurityPortalNotifier();
  ~SecurityPortalNotifier();

  void set_sms_manager(SecurityPortalSMSManager* sms_manager);
  void set_mail_manager(SecurityPortalMailManager* mail_manager);

  void send_sms_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh);
  void send_email_to_all(std::string message, std::string subject, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh);
  void send_push_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh);
  
  // WhatsApp API is only available for businesses. Not worth it then.
  //void send_WA_to_all(std::string message, std::string incoming_connection_address, CasaMartinezLogger* cl, SecurityPortalDatabaseHelper* dh);

private:
  SecurityPortalSMSManager* _sms_manager;
  SecurityPortalMailManager* _mail_manager;
};
#endif
