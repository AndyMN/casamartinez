#ifndef SECURITYPORTALCONNECTIONFACTORY_H
#define SECURITYPORTALCONNECTIONFACTORY_H

#include "Poco/Net/TCPServerConnectionFactory.h"
#include "SecurityPortalConnection.h"
#include "SecurityPortalNotifier.h"

template <class ServerConnection>
class SecurityPortalConnectionFactory : public Poco::Net::TCPServerConnectionFactory
{
public:
	SecurityPortalConnectionFactory(std::string aes_key_filename, std::string database_location, Poco::Net::SocketAddress authserver_sa, SecurityPortalNotifier* notifier)
		:_aes_key_filename(aes_key_filename),
		 _database_location(database_location),
		 _authserver_sa(authserver_sa),
		 _notifier(notifier){}

	~SecurityPortalConnectionFactory(){}

	SecurityPortalConnection* createConnection(const Poco::Net::StreamSocket& socket){
		return new ServerConnection(socket, _aes_key_filename, _database_location, _authserver_sa, _notifier);
	}

private:
	std::string _database_location;
	std::string _aes_key_filename;
	Poco::Net::SocketAddress _authserver_sa;
	SecurityPortalNotifier* _notifier;
};



#endif // SECURITYPORTALCONNECTIONFACTORY_H
