#ifndef SECURITYPORTALSMSMANAGER_H
#define SECURITYPORTALSMSMANAGER_H


#include<string>


class SecurityPortalSMSManager{

public:
  SecurityPortalSMSManager(std::string account_sid, std::string auth_token, std::string from_tel_number);
  ~SecurityPortalSMSManager();

  void send_sms(std::string to_tel_number, std::string message);

private:
  std::string _account_sid;
  std::string _auth_token;
  std::string _from_tel_number;
};
#endif
