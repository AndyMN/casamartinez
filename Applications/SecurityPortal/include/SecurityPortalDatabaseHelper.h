#ifndef SECURITYPORTALDATABASEHELPER_H
#define SECURITYPORTALDATABASEHELPER_H

#include "DatabaseHelper.h"

class SecurityPortalDatabaseHelper: public DatabaseHelper{
public:
  SecurityPortalDatabaseHelper(std::string database_location);
  ~SecurityPortalDatabaseHelper();

  std::string get_location_of_sensor(std::string rx_address);
  void add_entry_sensor_states(std::string rx_address, std::string location, int sensor_state);

  std::vector<std::string> get_sensor_locations();
  std::pair<int, int> get_latest_entry_of_location(std::string location);

  bool location_in_database(std::string location);
  void change_location(std::string old_location, std::string new_location);

  bool tel_number_in_database(std::string tel_number);
  void change_tel_number(std::string old_tel_number, std::string new_tel_number);

  bool email_in_database(std::string email);
  void change_email(std::string old_email, std::string new_email);

  bool name_in_database(std::string name);

  void add_contact(std::string name, std::string tel_number, std::string email);

  bool alarm_is_on();
  void add_entry_alarm_state(int alarm_state);

  std::vector<std::string> get_names();
  std::vector<std::string> get_email_addresses();
  std::vector<std::string> get_tel_numbers();
  std::vector<std::vector<std::string>> get_contact_info();





private:


};

#endif // SECURITYPORTALDATABASEHELPER_H
