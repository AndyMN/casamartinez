#include <iostream>
#include <vector>
#include <string>

#include <curl/curl.h>

#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPBasicCredentials.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "toml.hpp"


int main(int argc, char* argv[])
{

	if (argc < 2){
		std::cerr << "USAGE: " << argv[0] << " <CONFIG>" << std::endl;
		return 1;
	}

	auto config_toml = toml::parse(std::string(argv[1]));
	auto twilio_config = toml::find(config_toml, "twilio");
	std::string twilio_account_sid = toml::find<std::string>(twilio_config, "account_sid");
	std::string twilio_auth_token = toml::find<std::string>(twilio_config, "auth_token");
	std::string twilio_from_number = toml::find<std::string>(twilio_config, "phone_number");
	
	auto dummy_config = toml::find(config_toml, "dummy");
	std::string to_tel_nr = toml::find<std::string>(dummy_config, "to_tel_nr");
	std::string message = toml::find<std::string>(dummy_config, "message");

	// LIBCURL STYLE
	/*
	CURL* curl = curl_easy_init();

	if (curl){

		curl_easy_setopt(curl, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/" + twilio_account_sid + "/Messages.json");
		curl_easy_setopt(curl, CURLOPT_POST, 1);

		char* output1 = curl_easy_escape(curl, twilio_from_number, 0);
		char* output2 = curl_easy_escape(curl, twilio_to_nr, 0);
		char* output3 = curl_easy_escape(curl, "Yoyoyo", 0);

		std::string arguments = "From=" + std::string(output1) + "&To=" + std::string(output2) + "&Body=" + std::string(output3);

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, arguments.c_str());


		curl_easy_setopt(curl, CURLOPT_USERPWD, twilio_account_sid + ":" + twilio_auth_token);
		CURLcode res = curl_easy_perform(curl);

		std::cout << res << std::endl;

		curl_easy_cleanup(curl);
	}*/

	// POCO STYLE
	Poco::URI myURI("https://api.twilio.com/2010-04-01/Accounts/" + twilio_account_sid + "/Messages.json");

	Poco::Net::HTTPSClientSession myHTTPSSession(myURI.getHost(), myURI.getPort());

	Poco::Net::HTTPRequest myRequest(Poco::Net::HTTPRequest::HTTP_POST, myURI.getPath(), Poco::Net::HTTPMessage::HTTP_1_1);

	Poco::Net::HTTPBasicCredentials cred(twilio_account_sid, twilio_auth_token);
	cred.authenticate(myRequest);
	Poco::Net::HTMLForm myForm;
	myForm.add("From", twilio_from_number);
	myForm.add("To", to_tel_nr);
	myForm.add("Body", message);
	myForm.prepareSubmit(myRequest);

	std::ostream& myStream = myHTTPSSession.sendRequest(myRequest);
	myForm.write(myStream);

	Poco::Net::HTTPResponse response;
	std::istream& responseStream = myHTTPSSession.receiveResponse(response);

	Poco::StreamCopier sc;
	sc.copyStream(responseStream, std::cout);


  return 0;
}
