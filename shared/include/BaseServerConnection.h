#ifndef BASESERVERCONNECTION_H
#define BASESERVERCONNECTION_H

#include <vector>
#include <string>

#include "CryptoHelper.h"
#include "Poco/Net/TCPServerConnection.h"


class BaseServerConnection:public Poco::Net::TCPServerConnection
{
public:
	BaseServerConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename);
	~BaseServerConnection();

	virtual void run();

	std::string receive_data(const unsigned int buffer_size = 512);

	std::vector<std::string> read_received_data(std::string received_data, std::string delimiter = "\t");

	void send_data(std::string data_to_send);


protected:
	CryptoHelper _ch;
	std::vector<std::string> _split_string(std::string string_to_split, std::string delimiter);
};



#endif // BASESERVER_H
