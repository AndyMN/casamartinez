#ifndef CRYPTOHELPER_H
#define CRYPTOHELPER_H

#include <string>
#include <vector>

#include "Poco/Random.h"

class CryptoHelper{
public:
    CryptoHelper();
    CryptoHelper(std::string aes_key_filename);
    
    std::string decrypt_data(std::string data_to_decrypt);
    std::string encrypt_data(std::string data_to_encrypt);

    std::string generate_random_string(size_t length);
    std::vector<unsigned char> generate_random_bytevec(size_t length);
    std::string SHA256_hash_string_to_hex(std::string input_string);

    std::pair<std::string, std::string> generate_selector_validator_pair();
    std::string generate_validator();

    std::string encode_hex(std::vector<unsigned char> bytevec);
    std::string encode_hex(std::string input_string);
    std::vector<unsigned char> decode_hex(std::string hexstring);

    std::string encode_base64(std::vector<unsigned char> bytevec);
    std::vector<unsigned char> decode_base64(std::string base64string);

    std::string hash_password(std::string password);
    std::string hash_password(std::string password, std::string salt);

    bool validators_are_equal(std::string input_validator, std::string hashed_database_validator);
    bool passwords_are_equal(std::string input_pasword, std::string hashed_database_password);

    std::string string_from_bytevec(std::vector<unsigned char> bytevec);

    const std::string aes_key_hexstring() const;
    const size_t selector_length() const;
    const size_t validator_length() const;
    const size_t iv_size() const;
    const size_t tag_size() const;
    const size_t salt_size() const;
    const size_t hashed_password_size() const;

private:
    std::string _aes_key_filename;
    void _set_aes_key_from_file(); 

    static const size_t _aes_key_length = 32;  // Bytes
    std::vector<unsigned char> _aes_key_bytevec;
    std::string _aes_key_hexstring;

    static const size_t _selector_length = 12;  // Bytes
    static const size_t _validator_length = 12;  // Bytes 
    static const size_t _iv_size = 12;  // Bytes. The initial vector which should always be unique. Also called nonce.
    static const size_t _tag_size = 12;  // Bytes. Size of MAC/Tag obtained from GCM encryption and used for decryption.
    static const size_t _salt_size = 12;  // Bytes. Size of salt to be used for hashing passwords.
    static const size_t _hashed_password_size = 32;  // Bytes. Size of hashed password.

    Poco::Random _rng;
};
#endif