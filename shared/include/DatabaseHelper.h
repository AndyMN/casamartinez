#ifndef DATABASEHELPER_H
#define DATABASEHELPER_H

#include "sqlite3.h"
#include <string>
#include <vector>

class DatabaseHelper
{
public:

	DatabaseHelper(std::string database_location);
	~DatabaseHelper();

protected:

	int _connect_database();

	int _prepare_statement(const char* sql_command, int sql_command_maxlength = 200);

	int _bind_statement_text(int variable_position, const char* text_to_bind);
	int _bind_statement_int(int variable_position, int int_to_bind);

	int _execute_statement();

	int _finalize_statement();

	int _close_database_connection();


	sqlite3* _database;
	sqlite3_stmt* _statement;

private:
	std::string _database_location;
};




#endif // DATABASEHELPER_H
