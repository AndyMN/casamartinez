#ifndef CASAMARTINEZLOGGER_H
#define CASAMARTINEZLOGGER_H

#include <iostream>
#include <fstream>

#include "CasaMartinezException.h"


class CasaMartinezLogger
{
public:
	CasaMartinezLogger(std::string log_file_name, std::string error_log_file_name);
	~CasaMartinezLogger();

	void log(std::string text_to_log);
	void log_error(std::string error_to_log);

private:
	std::ofstream _log_file;
	std::ofstream _error_log_file;

	std::string _log_file_name;
	std::string _error_log_file_name;

	std::string _get_timestamp();
};







#endif // CASAMARTINEZLOGGER_H
