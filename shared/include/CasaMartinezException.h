#ifndef CASAMARTINEZEXCEPTION_H
#define CASAMARTINEZEXCEPTION_H

#include <exception>
#include <string>
#include <map>

class CasaMartinezException:public std::exception
{
public:
	CasaMartinezException(int err_code);
	CasaMartinezException(int err_code, int SQL_error_code);
	CasaMartinezException(std::string err_message);
	~CasaMartinezException();

	virtual const char* what() const throw();

	int get_error_code();
	int get_error_code_sql();

	std::string get_error_message();

private:
    int _error_code;
    int _SQL_error_code;

    std::string _init_err_message;

    static std::map<int, std::string> _error_code_messages;
};




#endif // CASAMARTINEZEXCEPTION_H
