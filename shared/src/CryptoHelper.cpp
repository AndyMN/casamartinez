#include "CryptoHelper.h"

#include <iostream>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "argon2.h"

#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/Cipher.h"
#include "Poco/Crypto/CipherKey.h"

#include "Poco/Crypto/CryptoStream.h"
#include "Poco/Crypto/CryptoTransform.h"
#include "Poco/Crypto/DigestEngine.h"

#include "Poco/Base64Encoder.h"
#include "Poco/Base64Decoder.h"

CryptoHelper::CryptoHelper(){
}

CryptoHelper::CryptoHelper(std::string aes_key_filename){
    if (std::filesystem::exists(aes_key_filename)){
        _aes_key_filename = aes_key_filename;
        _set_aes_key_from_file();
    }else{
        std::cerr << "AES key file with name " + aes_key_filename + " does not exist." << std::endl;
        exit(-1);
    }
}

std::string CryptoHelper::encrypt_data(std::string data_to_encrypt){

    // Generate a random IV
    std::vector<unsigned char> iv = this->generate_random_bytevec(_iv_size);

    // Encrypt data using AES 256 in GCM mode with generated iv
    Poco::Crypto::CipherKey key("aes-256-gcm", _aes_key_bytevec, iv);

	Poco::Crypto::Cipher* cipher = Poco::Crypto::CipherFactory::defaultFactory().createCipher(key);

    std::stringstream str;
    Poco::Crypto::CryptoTransform* encryptor = cipher->createEncryptor();
    Poco::Crypto::CryptoOutputStream encryptorStream(str, encryptor);
    encryptorStream << data_to_encrypt;
    encryptorStream.close();

    std::string tag = encryptor->getTag(_tag_size);
    std::string encrypted_data = str.str();


    std::string tag_hex = this->encode_hex(tag);
    std::string encrypted_data_hex = this->encode_hex(encrypted_data);
    std::string iv_hex = this->encode_hex(iv);

    std::string total_encrypted_data = encrypted_data_hex + iv_hex + tag_hex;

    return total_encrypted_data;
}

std::string CryptoHelper::decrypt_data(std::string data_to_decrypt){
    
    // Take Data and IV from the data string
    std::string tag_hex = data_to_decrypt.substr(data_to_decrypt.length() - 2 * _tag_size);
    std::string iv_hex = data_to_decrypt.substr(data_to_decrypt.length() - 2 * _iv_size - 2 * _tag_size, 2 * _iv_size);
    std::string encrypted_data_hex = data_to_decrypt.substr(0, data_to_decrypt.size() - 2 * _iv_size - 2 * _tag_size);

    // Convert the IV hexstring to bytevec
    std::vector<unsigned char> iv = this->decode_hex(iv_hex);
    std::vector<unsigned char> tag = this->decode_hex(tag_hex);
    std::string tag_string = this->string_from_bytevec(tag);

    Poco::Crypto::CipherKey key("aes-256-gcm", _aes_key_bytevec, iv);

	Poco::Crypto::Cipher* cipher = Poco::Crypto::CipherFactory::defaultFactory().createCipher(key);

    std::vector<unsigned char> encrypted_data = this->decode_hex(encrypted_data_hex);
    std::string encrypted_data_string = this->string_from_bytevec(encrypted_data);

    std::stringstream str(encrypted_data_string);
    Poco::Crypto::CryptoTransform* decryptor = cipher->createDecryptor();
	decryptor->setTag(tag_string);
	Poco::Crypto::CryptoInputStream decryptorStream(str, decryptor);
	std::string decrypted_data;
    std::string read_decryptor;

    while (!decryptorStream.eof()){
        decryptorStream >> read_decryptor;
        decrypted_data += read_decryptor + "\t";
    }
    decrypted_data = decrypted_data.substr(0, decrypted_data.size() - 1);

    return decrypted_data;
}


std::vector<unsigned char> CryptoHelper::generate_random_bytevec(size_t length){

    std::vector<unsigned char> random_bytevec(length);

    for (unsigned char& random_byte: random_bytevec){
        random_byte = _rng.nextChar();
    }

    return random_bytevec;
}

std::string CryptoHelper::generate_random_string(size_t length){
    std::vector<unsigned char> random_bytevec = this->generate_random_bytevec(length);

    return this->encode_hex(random_bytevec);
}

std::string CryptoHelper::SHA256_hash_string_to_hex(std::string input_string){

    Poco::Crypto::DigestEngine digest_engine("SHA256");
    
    digest_engine.update(input_string);

    return digest_engine.digestToHex(digest_engine.digest());
}

std::pair<std::string, std::string> CryptoHelper::generate_selector_validator_pair(){

    std::string selector = this->generate_random_string(_selector_length);
    std::string validator = this->generate_random_string(_validator_length);

    return std::make_pair(selector, validator);
}

std::string CryptoHelper::generate_validator(){
    return this->generate_random_string(_validator_length);
}

std::string CryptoHelper::encode_hex(std::vector<unsigned char> bytevec){

    std::stringstream hexstringstream;

    hexstringstream << std::hex;

    for (unsigned char byte: bytevec){
        hexstringstream << std::hex << std::setw(2) << std::setfill('0') << (int)byte;
    }
    
    return hexstringstream.str();
}

std::string CryptoHelper::encode_hex(std::string input_string){
    std::vector<unsigned char> bytevec;

    for (unsigned char byte: input_string){
        bytevec.push_back(byte);
    }

    return this->encode_hex(bytevec);
}

std::vector<unsigned char> CryptoHelper::decode_hex(std::string hexstring){
    std::vector<unsigned char> bytevec;

    for (int i = 0; i < hexstring.length(); i+=2){
        std::string bytestring = hexstring.substr(i, 2);
        unsigned char byte = (unsigned char) strtol(bytestring.c_str(), NULL, 16);
        bytevec.push_back(byte);
    }

    return bytevec;
}

std::string CryptoHelper::encode_base64(std::vector<unsigned char> bytevec){

    std::stringstream base64stringstream;

    Poco::Base64Encoder base64encoder(base64stringstream);
    for (unsigned char byte: bytevec){
        base64encoder << byte;
    }
    base64encoder.close();

    return base64stringstream.str();
}

std::vector<unsigned char> CryptoHelper::decode_base64(std::string base64string){

    std::stringstream base64stringstream(base64string);

    Poco::Base64Decoder base64decoder(base64stringstream);

    std::vector<unsigned char> bytevec;
    
    static const int eof = std::char_traits<char>::eof();
    int byte = base64decoder.get();
    while (byte != eof){
        bytevec.push_back((unsigned char) byte);
        byte = base64decoder.get();
    }

    return bytevec;
}

std::string CryptoHelper::hash_password(std::string password){

    std::string salt = this->generate_random_string(_salt_size);

    return this->hash_password(password, salt);
}

std::string CryptoHelper::hash_password(std::string password, std::string salt){

    std::vector<unsigned char> salt_bytevec = this->decode_hex(salt);
    char salt_array[_salt_size];
    for (int i = 0; i < salt_bytevec.size(); i++){
        salt_array[i] = salt_bytevec[i];
    }

    char hashed_password[_hashed_password_size];

    uint32_t t_cost = 20;            // 1-pass computation
    uint32_t m_cost = (1<<16) * (500.0 / 64.0);      // 64 mebibytes memory usage
    uint32_t parallelism = 2;       // number of threads and lanes

    // high-level API
    argon2d_hash_raw(t_cost, m_cost, parallelism, password.c_str(), password.length(), salt_array, _salt_size, hashed_password, _hashed_password_size);

    // Convert C array of bytes to vector of bytes
    std::vector<unsigned char> hashed_password_bytevec;
    for (int i = 0; i < _hashed_password_size; i++){
        hashed_password_bytevec.push_back(hashed_password[i]);
    }

    std::string hashed_password_hex = this->encode_hex(hashed_password_bytevec);

    std::string hashed_password_and_salt = hashed_password_hex + salt;

    return hashed_password_and_salt;
}

bool CryptoHelper::validators_are_equal(std::string input_validator, std::string hashed_database_validator){
    std::cout << "validators equal" << std::endl;
    std::cout << this->SHA256_hash_string_to_hex(input_validator) << std::endl;
    std::cout << hashed_database_validator << std::endl;
    return this->SHA256_hash_string_to_hex(input_validator) == hashed_database_validator;
}

bool CryptoHelper::passwords_are_equal(std::string input_pasword, std::string hashed_database_password){

    std::string salt = hashed_database_password.substr(hashed_database_password.size() - 2 * _salt_size);

    std::string hashed_input_password = this->hash_password(input_pasword, salt);

    return hashed_input_password == hashed_database_password;
}

std::string CryptoHelper::string_from_bytevec(std::vector<unsigned char> bytevec){
    std::string result;
    for (unsigned char byte: bytevec){
        result.push_back(byte);
    }

    return result;
}

const std::string CryptoHelper::aes_key_hexstring()const{
    return _aes_key_hexstring;
}

const size_t CryptoHelper::selector_length() const{
    return _selector_length;
}

const size_t CryptoHelper::validator_length() const{
    return _validator_length;
}

const size_t CryptoHelper::iv_size() const{
    return _iv_size;
}

const size_t CryptoHelper::tag_size() const{
    return _tag_size;
}

const size_t CryptoHelper::salt_size() const{
    return _salt_size;
}

void CryptoHelper::_set_aes_key_from_file(){
    std::ifstream aes_key_file(_aes_key_filename);
    
    if (aes_key_file.is_open()){
        std::getline(aes_key_file, _aes_key_hexstring);
    }

    aes_key_file.close();

    _aes_key_bytevec = this->decode_hex(_aes_key_hexstring);
} 