#include "CasaMartinezLogger.h"

#include <time.h>

CasaMartinezLogger::CasaMartinezLogger(std::string log_file_name, std::string error_log_file_name)
	:_log_file_name(log_file_name),
	 _error_log_file_name(error_log_file_name){
}

CasaMartinezLogger::~CasaMartinezLogger(){
	if (_log_file.is_open()){
		_log_file.close();
	}

	if (_error_log_file.is_open()){
		_error_log_file.close();
	}
}


void CasaMartinezLogger::log(std::string text_to_log){

	std::string time_stamp = _get_timestamp();

	_log_file.open(_log_file_name, std::ofstream::out | std::ofstream::app);

	_log_file << time_stamp << "\t" << text_to_log << "\n";

	_log_file.close();
}

void CasaMartinezLogger::log_error(std::string error_to_log){

	std::string time_stamp = _get_timestamp();

	_error_log_file.open(_error_log_file_name, std::ofstream::out | std::ofstream::app);

	_error_log_file << time_stamp << "\t" << error_to_log << "\n";

	_error_log_file.close();
}

std::string CasaMartinezLogger::_get_timestamp(){
	time_t rawtime;
	struct tm* timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	std::string time_stamp_newlined = asctime(timeinfo);

	std::string time_stamp = time_stamp_newlined.substr(0, time_stamp_newlined.size() - 1);

	return time_stamp;
}
