#include "DatabaseHelper.h"

#include <iostream>
#include <cstring>
#include <filesystem>

#include "CasaMartinezException.h"

#include "sqlite3.h"

DatabaseHelper::DatabaseHelper(std::string database_location){

	if (std::filesystem::exists(database_location)){
		_database_location = database_location;
		this->_connect_database();
	}else{
		std::cerr << "Database file with name " + database_location + " does not exist." << std::endl;
        exit(-1);
	}
}

DatabaseHelper::~DatabaseHelper(){

	this->_close_database_connection();
}

int DatabaseHelper::_connect_database(){

	int execute_status = sqlite3_open(_database_location.c_str(), &_database);

	if (execute_status != SQLITE_OK){

		std::cerr << "Can't open database in location: " << _database_location << std::endl;
		std::cerr << "Error code: " << execute_status << std::endl;
		_close_database_connection();

		exit(execute_status);
	}

	return execute_status;
}

int DatabaseHelper::_close_database_connection(){

		int execute_status = sqlite3_close_v2(_database);

		return execute_status;
}

int DatabaseHelper::_finalize_statement(){

	int execute_status = sqlite3_finalize(_statement);

	if (execute_status != SQLITE_OK){
		throw CasaMartinezException(12, execute_status);
	}

	return execute_status;
}

int DatabaseHelper::_execute_statement(){

	int execute_status = sqlite3_step(_statement);

	if (execute_status != SQLITE_ROW && execute_status != SQLITE_DONE){
		throw CasaMartinezException(13, execute_status);
	}

	return execute_status;
}

int DatabaseHelper::_prepare_statement(const char* sql_command, int sql_command_maxlength){

	int execute_status = sqlite3_prepare_v2(_database, sql_command, sql_command_maxlength, &_statement, NULL);

	if (execute_status != SQLITE_OK){
		throw CasaMartinezException(14, execute_status);
	}

	return execute_status;
}


int DatabaseHelper::_bind_statement_text(int variable_position, const char* text_to_bind){

	int execute_status = sqlite3_bind_text(_statement, variable_position, text_to_bind, -1, SQLITE_STATIC);

	if (execute_status != SQLITE_OK){
		throw CasaMartinezException(15, execute_status);
	}
	return execute_status;
}

int DatabaseHelper::_bind_statement_int(int variable_position, int int_to_bind){

	int execute_status = sqlite3_bind_int(_statement, variable_position, int_to_bind);

	if (execute_status != SQLITE_OK){
		throw CasaMartinezException(16, execute_status);
	}
	return execute_status;
}
