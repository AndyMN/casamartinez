#include "CasaMartinezException.h"

CasaMartinezException::CasaMartinezException(std::string err_message)
    :_error_code(-1),
     _SQL_error_code(-1),
     _init_err_message(err_message){
}

CasaMartinezException::CasaMartinezException(int err_code)
    :_error_code(err_code),
     _SQL_error_code(-1){
}

CasaMartinezException::CasaMartinezException(int err_code, int SQL_error_code)
    :_error_code(err_code),
     _SQL_error_code(SQL_error_code){
}

CasaMartinezException::~CasaMartinezException(){
}


const char* CasaMartinezException::what() const throw(){

	return "Exception happened";

}

int CasaMartinezException::get_error_code(){
	return _error_code;
}

std::string CasaMartinezException::get_error_message(){
	if (_SQL_error_code < 0 && _error_code > 0){
		return _error_code_messages[_error_code];
	}else if (_SQL_error_code >= 0 && _error_code >0){
		return _error_code_messages[_error_code] + " SQL Code: " + std::to_string(_SQL_error_code);
	}else if (_SQL_error_code < 0 && _error_code < 0){
		return _init_err_message;
	}else{
		return "Error not defined.";
	}
}


int CasaMartinezException::get_error_code_sql(){
	return _SQL_error_code;
}

std::map<int, std::string> CasaMartinezException::_error_code_messages = {{1, "Login: Invalid auth method."},
    {2, "Login: User name not in database."}, {3, "Login: Password does not match."},
    {4, "Login: Selector not in database."}, {5, "Login: Validator does not match."},
    {6, "Database: Could not create auth token."}, {7, "Database: Could not create user account."},
    {8, "Database: Could not remove auth token."}, {9, "Database: Could not renew auth token."},
    {10, "Database: Could not find selector."}, {11, "Database: Could not find user name"},
    {12, "Database: Could not finalize statement."}, {13, "Database: Could not execute statement."},
    {14, "Database: Could not prepare statement."}, {15, "Database: Could not bind text to statement."},
    {16, "Database: Could not bind int to statement."}, {17, "Base: Received data is not correctly sized."},
    {18, "Crypto: Datasize is not a multiple of blocksize."}, {19, "Crypto: Data integrity compromised."},
    {20, "Login: App name not in database."}, {21, "ServerConnection: Response not recognized."},
    {22, "Security: No request."}, {23, "Security: Request not recognized."}, {24, "Base: Invalid package."},
    {25, "Security: RX Address length invalid."}, {26, "Security: Sensor state invalid."},
    {27, "Security: RX Address not found in locations."}, {28, "Security: Could not create sensor state entry."},
    {29, "Security: No alarm state found."}, {30, "Security: Alarm state in DB is invalid."},
    {31, "Security: No email addresses stored in DB."}, {32, "Security: No tel number stored in DB."},
    {33, "Security: No alarm state in request."}, {34, "Security: Invalid alarm state in request."},
    {35, "Security: No contact info specifiers."}, {36, "Security: Invalid contact info specifier."},
    {37, "Security: Tel number not in DB."}, {38, "Security: Email not in DB."},
    {39, "Security: No locations stored in DB."}, {40, "Security: Could not change Tel number."},
    {41, "Security: Could not change email."}, {42, "Security: Could not create alarm state entry."},
    {43, "Security: No names stored in contact info"}, {44, "Security: No old/new location data present."},
    {45, "Security: Could not change sensor location."}, {46, "Security: Old location not in DB."},
    {47, "Security: Not enough contact info in request."}, {48, "Security: Email already in DB."},
    {49, "Security: Tel number already in DB."}, {50, "Security: Name already in DB."},
    {51, "Security: Could not add contact."}, {52, "Security: Invalid SMS message."},
    {53, "BaseServer: No received data."}, {54, "BaseServer: Received data is not long enough to contain IV."},
    {55, "Security: Invalid master node request."}, {56, "Login: Not enough credentials sent."}};
