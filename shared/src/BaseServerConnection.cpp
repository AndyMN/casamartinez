#include <iostream>

#include "BaseServerConnection.h"
#include "CasaMartinezException.h"

BaseServerConnection::BaseServerConnection(const Poco::Net::StreamSocket& s, std::string aes_key_filename)
	:Poco::Net::TCPServerConnection(s),
	 _ch(aes_key_filename){
}

BaseServerConnection::~BaseServerConnection(){}

void BaseServerConnection::run(){

	std::cout << "New connection from: " << this->socket().peerAddress().host().toString() << std::endl;

	try{
		// Base Server is an Echo Server
		std::string received_data = receive_data();

		std::vector<std::string> split_data = read_received_data(received_data);

		for (unsigned int i = 0; i < split_data.size(); i++){

			std::cout << split_data[i] << std::endl;
		}

		send_data(split_data[split_data.size() - 1]);
	}catch (Poco::Exception& exc){
		std::cerr << "ServerConnection: " << exc.displayText() << std::endl;
	}

}

std::string BaseServerConnection::receive_data(const unsigned int buffer_size){

	char buffer[buffer_size];
	memset(buffer, 0, buffer_size);
	int n = this->socket().receiveBytes(buffer, sizeof(buffer));

	if (n > 0){
		// Received text is what we get from TCP
		std::string received_data(buffer, n);

		return received_data;
	}else{
		return "";
	}
}

std::vector<std::string> BaseServerConnection::read_received_data(std::string received_data, std::string delimiter){

	if (received_data.size() > 0){

		if (received_data.length() < (2 * (_ch.iv_size() + _ch.tag_size()))){
			// Received data not long enough to contain IV. Throw
			throw CasaMartinezException(54);
		}else{
			// Decrypt the encrypted data
			std::string decrypted_data = _ch.decrypt_data(received_data);

			// Split data into chunks
			std::vector<std::string> split_received_data = _split_string(decrypted_data, delimiter);
			split_received_data.push_back(decrypted_data);

			return split_received_data;
		}
	}else{
		// No received data. Throw.
		throw CasaMartinezException(53);
	}
}

std::vector<std::string> BaseServerConnection::_split_string(std::string string_to_split, std::string delimiter){

	std::vector<std::string> string_parts;
	std::string _string_to_split = string_to_split;

	// Find first occurrence of delimiter
	size_t delimiter_pos = _string_to_split.find(delimiter);

	// As long as there is a delimiter keep splitting and pushing the parts into the string vector container
	while (delimiter_pos != std::string::npos){
		string_parts.push_back(_string_to_split.substr(0, delimiter_pos));
		_string_to_split = _string_to_split.substr(delimiter_pos + 1);
		delimiter_pos = _string_to_split.find(delimiter);
	}
	if (delimiter_pos == std::string::npos){

		string_parts.push_back(_string_to_split);
	}

	return string_parts;

}

void BaseServerConnection::send_data(std::string data_to_send){
	std::string encrypted_data = _ch.encrypt_data(data_to_send);

	this->socket().sendBytes(encrypted_data.data(), encrypted_data.length());
}
