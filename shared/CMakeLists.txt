
add_library(CasaMartinezShared SHARED  ${CMAKE_SOURCE_DIR}/shared/src/CryptoHelper.cpp
                                       ${CMAKE_SOURCE_DIR}/shared/src/BaseServerConnection.cpp
                                       ${CMAKE_SOURCE_DIR}/shared/src/CasaMartinezException.cpp
                                       ${CMAKE_SOURCE_DIR}/shared/src/CasaMartinezLogger.cpp
                                       ${CMAKE_SOURCE_DIR}/shared/src/DatabaseHelper.cpp)

set_target_properties(CasaMartinezShared PROPERTIES CXX_STANDARD 17)
target_link_libraries(CasaMartinezShared PUBLIC stdc++fs)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/shared/include)

# Add External Poco Dependency
target_link_libraries(CasaMartinezShared PUBLIC PocoCrypto PocoNet)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/Poco/Crypto/include)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/Poco/Net/include)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/Poco/Foundation/include)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/Poco/Util/include)

# Add External Sqlite Dependency
# Sets the -fPIC flag. Necesary for adding a static library to a shared library
set_property(TARGET SQLite3 PROPERTY POSITION_INDEPENDENT_CODE ON)  
target_link_libraries(CasaMartinezShared PUBLIC SQLite3)
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/sqlite)

# Add External Argon2 Dependency
target_link_libraries(CasaMartinezShared PUBLIC Argon2)

# Add External toml dependency
target_include_directories(CasaMartinezShared PUBLIC ${CMAKE_SOURCE_DIR}/external/toml11)